<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemCreateRequest;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * @var Item
     */
    private $model;

    /**
     * @param Item $model
     */
    public function __construct(Item $model)
    {
        $this->model = $model;
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): \Illuminate\Http\JsonResponse
    {
        try {
            $data = $this->model->all();

            return response()->json([
                'success' => 1,
                'type'    => 'success',
                'items'   => $data
            ], 200);
        } catch (\Exception $exception) {
            Log::info($exception);

            return response()->json([
                'success' => 0,
                'type'    => 'error',
                'message' => $exception->getMessage()
            ], $exception->getCode());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ItemCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ItemCreateRequest $request): \Illuminate\Http\JsonResponse
    {
        try {
            $data = $request->all();
            $this->model->create($data);

            return response()->json([
                'success' => 1,
                'type'    => 'success',
                'message' => 'Item added'
            ], 201);
        } catch (\Exception $exception) {
            Log::info($exception);

            return response()->json([
                'success' => 0,
                'type'    => 'error',
                'message' => $exception->getMessage()
            ], $exception->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Item $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Item $item): \Illuminate\Http\JsonResponse
    {
        try {
            $data = $request->all();
            $item->update($data);

            return response()->json([
                'success' => 1,
                'type'    => 'success',
                'message' => 'Item updated'
            ], 200);
        } catch (\Exception $exception) {
            Log::info($exception);

            return response()->json([
                'success' => 0,
                'type'    => 'error',
                'message' => $exception->getMessage()
            ], $exception->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Item $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Item $item): \Illuminate\Http\JsonResponse
    {
        try {
            $item->delete();

            return response()->json([
                'success' => 1,
                'type'    => 'success',
                'message' => 'Deleted'
            ], 200);
        } catch (\Exception $exception) {
            Log::info($exception);

            return response()->json([
                'success' => 0,
                'type'    => 'error',
                'message' => $exception->getMessage()
            ], $exception->getCode());
        }
    }
}
